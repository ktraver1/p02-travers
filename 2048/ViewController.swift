//
//  ViewController.swift
//  2048
//
//  Created by kevin travers on 2/5/16.
//  Copyright © 2016 Bunny Phantom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var highScore: UILabel!
    @IBOutlet weak var score: UILabel!
    
   
    @IBOutlet var grid: [UILabel]!
    
    
    var currentScore:Int = 0
    var matrix = Array(count: 4, repeatedValue: Array(count: 4, repeatedValue: 0))
    var gameOver:Bool = false
    var isWinner:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
       
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        self.view.addGestureRecognizer(swipeDown)
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        var swipeUp = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        self.view.addGestureRecognizer(swipeUp)
        newGame(self)
        
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                //print("Swiped right")
                 moveMatrix(true)
                //print(matrix)
            case UISwipeGestureRecognizerDirection.Down:
                //print("Swiped down")
                moveMatrixUpDown(true)
                //print(matrix)
            case UISwipeGestureRecognizerDirection.Left:
                //print("Swiped left")
                moveMatrix(false)
                //print(matrix)
            case UISwipeGestureRecognizerDirection.Up:
                //print("Swiped up")
                moveMatrixUpDown(false)
                //print(matrix)
            default:
                break
            }
            if isWinner{
                let alertController = UIAlertController(title: "YOU WON!!!", message:
                    "WINNING SCORE:\(currentScore)", preferredStyle: UIAlertControllerStyle.Alert)
                
                
                alertController.addAction(UIAlertAction(title: "Try Again?", style: .Default,handler:{ action in
                    //run your function here
                    self.newGame(self)
                }))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                isWinner = false
                gameOver = false
                showMatrix()
               
            }else if gameOver{
                let alertController = UIAlertController(title: "GAME OVER", message:
                    "Final SCORE:\(currentScore)", preferredStyle: UIAlertControllerStyle.Alert)
                
                
                alertController.addAction(UIAlertAction(title: "Try Again?", style: .Default,handler:{ action in
                    //run your function here
                    self.newGame(self)
                }))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                gameOver = false
                showMatrix()
            }
            else{
                addRandom()
                showMatrix()
            }
        }
    }
    func moveMatrixUpDown(reverse: Bool){
        //print(matrix)
        reverseMatrix()
        
        moveMatrix(reverse)
        reverseMatrix()
        //print(matrix)
        
    }
    func reverseMatrix()
    {
        var counterRow = 0
        var counterCol = 0
        for row in matrix{
            for col in row{
                
                matrix[counterCol][counterRow] = col
                counterCol++
            }
            counterCol = 0
            counterRow++
        }
    }
    
    func moveMatrix(reverse: Bool){
        //print(matrix)
        var index: Int = 0
        var tempIndex: Int = 0
        var counter: Int = 0
        var mergeArray: Array = [0,0,0,0]
        //var tempArray:Array = Array(count: 4, repeatedValue: 0)
        var anyValidMoves:Bool = false
        for var tile in matrix{
            index = 0
           
            if (reverse){
                
                tile = tile.reverse()
                
            }
            
           var tempArray:Array = tile
            mergeArray = [0,0,0,0]
            for element in tile{
                
                if isValidTile(element){
                    tempIndex = index
                    while(isValidMove(tile, index: tempIndex)){
                        anyValidMoves = true
                        
                       tempArray[tempIndex] = 0
                       tempArray[tempIndex-1] = element
                       tempIndex--
                        
                    }
                    if tempIndex != 0
                    {
                        if tempIndex != 0{
                            if tempArray[tempIndex-1] == element{
                                anyValidMoves = true
                                
                                if (mergeArray[tempIndex-1] == 0){
                                
                                    tempArray[tempIndex-1] = 2 * element
                                    tempArray[tempIndex] = 0
                                    mergeArray[tempIndex-1] = 1
                                    currentScore += (2 * element)
                                    score.text = "\(currentScore)"
                                }
                            
                            }
                            //else if tempIndex != 3{
                              // if tempArray[tempIndex+1] == element{
                                //    anyValidMoves = true
                                //}
                            //}
                        }
                    }
                    
                    
                    tile = tempArray
                    
                }
                
                if(reverse){
                    tempArray = tempArray.reverse()
                }
                updateMatrix(tempArray, index: counter)
                index++
                if(reverse){
                    tempArray = tempArray.reverse()
                }
            }
            
            counter++
        }
        
        if !anyValidMoves{
            
            reverseMatrix()
            counter = 0
            
            for row in matrix{
                counter = 0
                for col in row{
                    if counter != 3{
                        if (row[counter+1] == col){
                            anyValidMoves = true
                        }
                        counter++
                    }
                   
                }
            }
            reverseMatrix()
        }
        if !anyValidMoves{
            gameOver = true
        }
        
        //print(matrix)
    }
    
    func updateMatrix(arr: [Int], index:Int){
       matrix.removeAtIndex(index)
        matrix.insert(arr, atIndex: index)
       // print("arr insert")
        //print(arr)
        //for i in 0...3{
        
          //  matrix[index][i] = arr[i]
        //}
        
    }
    //check if current tile has value or not
    func isValidTile(tile: Int)->Bool{
        var result = false
        if tile != 0{
            result = true
        }
        return result
    }
    //check if next spot is empty or edge of the map
    //index current idex of item
    //[2,0,0,0]
    //0002
    func isValidMove(arr: [Int], index: Int)->Bool{
         var result = false
        //solves edgecase
        
        if isValidTile(index){
            if arr[index-1] == 0{
               result = true
            }
            
        }
        return result
    }
   
  
    
 
    
    @IBAction func newGame(sender: AnyObject) {
        
        checkHighScore()
        currentScore = 0
        score.text = "\(currentScore)"
        clearMatrix()
        addRandom()
        addRandom()
        showMatrix()
    }
    func checkHighScore(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("HighScore") == nil{
            defaults.setInteger(currentScore, forKey: "HighScore")
            highScore.text = "\(0)"
        }else{
            var highScoreNum = defaults.objectForKey("HighScore") as! Int
            if highScoreNum < currentScore{
                defaults.setInteger(currentScore, forKey: "HighScore")
                highScoreNum = currentScore
                
            }
            highScore.text = "\(highScoreNum)"
        }
        
    }
    func clearMatrix(){
        matrix = Array(count: 4, repeatedValue: Array(count: 4, repeatedValue: 0))
    }
    func addRandom(){
        var randomNumX = Int(arc4random_uniform(4))
        var randomNumY = Int(arc4random_uniform(4))
        var isSpace:Bool = false
        for row in matrix{
            for col in row{
                if (col == 0){
                    isSpace = true
                }
            }
        }
        if isSpace{
            while (matrix[randomNumX][randomNumY] != 0){
                randomNumX = Int(arc4random_uniform(4))
                randomNumY = Int(arc4random_uniform(4))
            }
            matrix[randomNumX][randomNumY] = 2
        }
       
    }
    func showMatrix(){
        //print("MATRIX HERE")
       var counter = 0
        
        for row in matrix{
            //print(row)
            for col in row{
                grid[counter].text = "\(col)"
                
                counter++
            }
        }
        for i in 0..<16{
            if grid[i].text == "0"
            {
                grid[i].text = ""
                grid[i].backgroundColor = UIColor.whiteColor()
            }else{
                grid[i].backgroundColor = UIColor.redColor()
            }
            /*
            else if grid[i].text == "2"
            {
                grid[i].backgroundColor = UIColor.lightGrayColor()
            }
            else if grid[i].text == "4"
            {
                grid[i].backgroundColor = UIColor.grayColor()
            }
            else if grid[i].text == "8"
            {
                grid[i].backgroundColor = UIColor.darkGrayColor()
            }
            else if grid[i].text == "16"
            {
                grid[i].backgroundColor = UIColor.purpleColor()
            }
            else if grid[i].text == "32"
            {
                grid[i].backgroundColor = UIColor.orangeColor()
            }
            else if grid[i].text == "64"
            {
                grid[i].backgroundColor = UIColor.greenColor()
            }
            else if grid[i].text == "128"
            {
                grid[i].backgroundColor = UIColor.blueColor()
            }
            else if grid[i].text == "256"
            {
                grid[i].backgroundColor = UIColor.yellowColor()
            }
            else if grid[i].text == "512"
            {
                grid[i].backgroundColor = UIColor.redColor()
            }
            else if grid[i].text == "1024"
            {
                grid[i].backgroundColor = UIColor.brownColor()
            }*/
            if grid[i].text == "2048"
            {
                grid[i].backgroundColor = UIColor.cyanColor()
                isWinner = true
            }
            
            
            
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}

